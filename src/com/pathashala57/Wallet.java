package com.pathashala57;
//represents container of money

class Wallet {

    private Money rupees;

    Wallet(Money money) {
        this.rupees = money;

    }

    Money balance() {
        return rupees;
    }

    void add(Money moneyToBeAdded) {
        rupees.add(moneyToBeAdded);
    }

    public boolean withdraw(Money moneyTowithdraw) {

        return rupees.sub(moneyTowithdraw);
    }
}
