package com.pathashala57;
//represents units of currency

public class Money {

    private int value;

    Money(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object that) {
        if (!(that instanceof Money))
            return false;
        Money other = (Money) that;
        return this.value == other.value;
    }

    void add(Money moneyToBeAdded) {
        this.value += moneyToBeAdded.value;
    }

    public boolean sub(Money moneytosubtract) {
        if (this.value >= moneytosubtract.value) {
            this.value -= moneytosubtract.value;
            return true;
        }
        return false;
    }

}
