package com.pathashala57;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestWallet {


    @Test
    void retunsTrueWhenMoneyEqual() {
        Money money = new Money(5);
        Money anotherMoney = new Money(5);
        assertTrue(money.equals(anotherMoney));


    }

    @Test
    void retunsFalseWhenMoneyNotComparedWithMoney() {
        Money money = new Money(5);
        assertFalse(money.equals(null));


    }

    @Test
    void testForAddMoney() {
        Money money = new Money(5);
        Money anotherMoney = new Money(5);
        money.add(anotherMoney);
        Money totalMoneyAfterAddition = new Money(10);
        assertTrue(totalMoneyAfterAddition.equals(money));


    }

    @Test
    void TestbalanceFunctionality() {
        Money money = new Money(5);
        Wallet myWallet = new Wallet(money);
        assertEquals(money, myWallet.balance());


    }

    @Test
    void TestAddMoneyFunctionality() {
        Money money = new Money(5);
        Money anotherMoney = new Money(5);
        Wallet myWallet = new Wallet(money);
        myWallet.add(anotherMoney);
        Money totalMoneyAfterAddition = new Money(10);
        assertTrue(totalMoneyAfterAddition.equals(myWallet.balance()));

    }

    @Test
    void testForWithdrawMoney() {
        Money money = new Money(5);
        Money anotherMoney = new Money(5);
        Wallet myWallet = new Wallet(money);
        myWallet.withdraw(anotherMoney);
        Money totalMoneyAfterWithdrawl = new Money(0);
        assertTrue(totalMoneyAfterWithdrawl.equals(myWallet.balance()));

    }

    @Test
    void testForWithdrawMoreMoneyThanInWallet() {
        Money money = new Money(5);
        Money moneyToWithdraw = new Money(6);
        Wallet myWallet = new Wallet(money);
        assertFalse(myWallet.withdraw(moneyToWithdraw));

    }


}
